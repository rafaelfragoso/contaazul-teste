import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {filter, isEmpty} from 'lodash';
import {addCar, applyFilters, removeFilters} from '../actions';
import CarList from '../components/CarList';

const getVisibleProducts = (products, filters) => {
  if ( isEmpty(filters) ) {
    return products;
  }

  return filter(products, filters);
};

const mapStateToProps = (state) => ({
  products: getVisibleProducts(state.products, state.visibilityFilter),
});

const matchDispatchToProps = (dispatch) => {
  return bindActionCreators({
    applyFilters,
    removeFilters,
    addCar
  }, dispatch);
};

export default connect(mapStateToProps, matchDispatchToProps)(CarList);
