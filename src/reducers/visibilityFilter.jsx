const visibilityFilter = (state = {}, action) => {
  switch (action.type) {
    case 'APPLY_FILTERS':
      return Object.assign({}, action.payload.filters);
    case 'REMOVE_FILTERS':
      return action.payload;
    default:
      return state;
  }
}

export default visibilityFilter;