import { combineReducers } from 'redux';
import visibilityFilter from './visibilityFilter';
import products from './products';
import { routerReducer } from 'react-router-redux';

const contaAzulApp = combineReducers({
  visibilityFilter,
  products,
  router: routerReducer
});

export default contaAzulApp;
