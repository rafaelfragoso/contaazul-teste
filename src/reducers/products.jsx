const products = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_CAR':
      return [...state, action.payload.car];
    default:
      return state;
  }
}

export default products;
