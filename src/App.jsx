import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'

import reducers from './reducers';
import './App.scss';
import CarContainer from './containers/CarContainer';
import Header from './components/Header';
import products from '../public/data.json';

const history = createHistory();
const middleware = routerMiddleware(history);

const initialState = Object.assign({}, products, {
  visibilityFilter: {}, 
});

const store = createStore(
  reducers,
  initialState,
  applyMiddleware(middleware)
);

class ContaAzulApp extends React.Component {
  render() {
    return (
      <div className="container">
        <Header />
        <CarContainer />
      </div>
    )
  }
}

ReactDOM.render( 
  <Provider store={store}>
    {}
    <ConnectedRouter history={history}>
      <div>
        <Route exact path="/" component={ContaAzulApp} />
      </div>
    </ConnectedRouter>
  </Provider>, 
  document.getElementById( 'contaazul-app' ) 
);

export default ContaAzulApp;
