import React from 'react';

export default class CarListItem extends React.Component {
  constructor(props) {
    super(props);  
  }

  render() {
    return (
      <tr>
        <th scope="row">{this.props.placa}</th>
        <td>{this.props.modelo}</td>
        <td>{this.props.marca}</td>
        <td>{this.props.imagem}</td>
        <td>{this.props.combustivel}</td>
        <td>{this.props.valor}</td>
      </tr>
    );
  }
}

