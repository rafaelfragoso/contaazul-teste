import React from 'react';

export default class Header extends React.Component {
  constructor(props) {
    super(props);  
  }

  render() {
    return (
      <div className="row">
        <div className="col">
          <nav className="navbar navbar-light bg-contaazul">
            <a className="navbar-brand" href="#">
              <img src="/images/logo-contaazul.svg" alt="Conta Azul" />
            </a>
          </nav>
        </div>
      </div>
    );
  }
}
