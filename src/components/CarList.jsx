import React from 'react';
import NumberFormat from 'react-number-format';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

export default class CarList extends React.Component {
  constructor(props) {
    super(props);  
  }

  imageFormatter(cell, row) {
    if (cell && cell.length > 0) {
      return `<a href='${cell}' target='_new'>Imagem</a>`;
    } else {
      return 'Sem Foto';
    }
  }

  priceFormatter(cell, row) {
    return <NumberFormat value={cell} displayType={'text'} thousandSeparator={'.'} decimalSeparator={','} decimalPrecision={true} prefix={''} />
  }

  onAddRow(row) {
    this.props.addCar(row);
  }

  createCustomInsertButton(onClick) {
    return (
      <InsertButton
        btnText='Novo Carro'
        btnContextual='btn-success'
        className='ca-btn-success'
        btnGlyphicon='' />
    );
  }

  render() {
    const options = {
      hideSizePerPage: true,
      sizePerPage: 5,
      alwaysShowAllBtns: true,
      withFirstAndLast: true,
      paginationSize: 3,
      onAddRow: this.onAddRow.bind(this),
      insertBtn: this.createCustomInsertButton
    };

    const selectRowProp = {
      mode: 'checkbox'
    };

    return (
      <BootstrapTable 
        ref="table"
        multiColumnSort={2}
        data={this.props.products} 
        selectRow={ selectRowProp }
        insertRow={true}
        pagination
        search
        searchPlaceholder='Pesquisar'
        options={options}
      >
        <TableHeaderColumn isKey dataField='placa'>Placa</TableHeaderColumn>
        <TableHeaderColumn dataField='modelo' dataSort={ true }>Modelo</TableHeaderColumn>
        <TableHeaderColumn dataField='marca' dataSort={ true }>Marca</TableHeaderColumn>
        <TableHeaderColumn dataField='imagem' dataFormat={this.imageFormatter}>Foto</TableHeaderColumn>
        <TableHeaderColumn dataField='combustivel' dataSort={ true }>Combustível</TableHeaderColumn>
        <TableHeaderColumn dataField='valor' dataFormat={this.priceFormatter} dataSort={ true }>Valor</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}

